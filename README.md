# Network Dilemma with Inertia

## Description
Agents are playing the following tragedy of the commons: each turn, they can choose to extract moderately (cooperate) or intensively (defect) natural resources from their local environment. These resources will then diffuse spatially (with a local or global topology, depending on the "local?" variable). Occasionally, they will copy the behaviour of one of their neighbours if it scores higher than themselves (+ random mistakes for ergodicity).
Natural resources ("wealth") are updated with a frequency rho_p.
Personnal wealth ("score") is updated with a frequency rho_t.
One can also model the delayed effect of human intervention by increasing "inertia" (ie. an agent's land resources won't be updated with respect to her current action, but to the one she took long ago).


## Result examples & Visuals


## Roadmap
This project is finished.


## Installation & Developpement
This works on Netlogo 6.2
The OM file is used for OpenMole model analysis (see https://openmole.org/)

## Authors and License
Developped by Arsene PIERROT-VALROFF, open-source, free of use.
